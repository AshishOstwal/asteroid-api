function extractData(asteroids) {
  const extractedData = [];
  asteroids.forEach((asteroid) => {
    const asteroidData = {};
    asteroidData.id = asteroid.id;
    asteroidData.name = asteroid.name.length > 20 ? (`${asteroid.name.slice(0, 20)}...`) : asteroid.name;
    asteroidData.distance = asteroid.estimated_diameter.kilometers.estimated_diameter_min;
    asteroidData.isHazardous = asteroid.is_potentially_hazardous_asteroid;
    asteroidData.observationDate = asteroid.orbital_data.first_observation_date;
    extractedData.push(asteroidData);
  });
  return extractedData;
}

function extractCloseApproachData(asteroids, closeApproachDateAsteroids) {
  asteroids.forEach((asteroid) => {
    const asteroidData = {};
    asteroidData.id = asteroid.id;
    asteroidData.name = asteroid.name.length > 20 ? (`${asteroid.name.slice(0, 20)}...`) : asteroid.name;
    asteroidData.distance = asteroid.estimated_diameter.kilometers.estimated_diameter_min;
    asteroidData.isHazardous = asteroid.is_potentially_hazardous_asteroid;
    asteroidData.observationDate = asteroid.orbital_data.first_observation_date;
    asteroidData.closeApproachDate = asteroid.close_approach_data[0].close_approach_date_full;
    closeApproachDateAsteroids.push(asteroidData);
  });
}

function sortData(nearEarthObjs) {
  const closeApproachDateAsteroids = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const date of Object.keys(nearEarthObjs)) {
    extractCloseApproachData(nearEarthObjs[`${date}`], closeApproachDateAsteroids);
  }
  closeApproachDateAsteroids.sort((a, b) => {
    const dateA = new Date(a.closeApproachDate).getTime();
    const dateB = new Date(b.closeApproachDate).getTime();
    return dateA > dateB ? 1 : -1;
  });
  return closeApproachDateAsteroids;
  // return closeApproachDateAsteroids;
}

export {
  extractData,
  extractCloseApproachData,
  sortData,
};
