// import VuetableFieldHandle from 'vuetable-2/src/components/VuetableFieldHandle.vue'

export default [
  // {
  //   name: VuetableFieldHandle
  // },
  'ID',
  'Name',
  {
    name: 'Distance',
    sortField: 'distance',
  },
  'Hazardous',
  'First observation date',
];
