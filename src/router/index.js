import Vue from 'vue';
import VueRouter from 'vue-router';
import { auth } from '../../firebase';
import Dashboard from '../views/Dashboard.vue';
import SavedAsteroid from '../views/SavedAsteroids.vue';
import AsteroidsByDate from '../views/AsteroidsByDate.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/searchByDate',
    name: 'SearchByDate',
    component: AsteroidsByDate,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/savedAsteroids',
    name: 'Saved',
    component: SavedAsteroid,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((x) => x.meta.requiresAuth);

  if (requiresAuth && !auth.currentUser) {
    next('/login');
  } else {
    next();
  }
});

export default router;
