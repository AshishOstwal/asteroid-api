import Vue from 'vue';
import Vuex from 'vuex';
import * as fb from '../../firebase';
import router from '../router';
import { auth, usersCollection } from '../../firebase';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    userProfile: {},
    savedAsteroids: [],
    asteroidIdsObj: {},
  },
  mutations: {
    setUserProfile(state, val) {
      state.userProfile = val;
    },
    setSavedAsteroids(state, [savedAsteroids, asteroidIdsObj]) {
      state.savedAsteroids = savedAsteroids;
      state.asteroidIdsObj = asteroidIdsObj;
    },
  },
  actions: {
    async login({ dispatch }, form) {
      const { user } = await fb.auth.signInWithEmailAndPassword(form.email, form.password);
      dispatch('fetchUserProfile', user);
    },

    async signup({ dispatch }, form) {
      const { user } = await fb.auth.createUserWithEmailAndPassword(form.email, form.password);

      await fb.usersCollection.doc(user.uid).set({
        name: form.name,
      });
      dispatch('fetchUserProfile', user);
    },

    async logout({ commit }) {
      await fb.auth.signOut();
      commit('setUserProfile', {});
      router.push('/login');
    },
    async fetchUserProfile({ commit }, user) {
      const userProfile = await fb.usersCollection.doc(user.uid).get();
      commit('setUserProfile', userProfile);
      if (router.currentRoute.path === '/login') {
        router.push('/');
      }
    },

    async fetchSavedAsteroids({ commit }) {
      const asteroidIdsObj = {};
      const savedAsteroids = [];
      usersCollection.doc(auth.currentUser.uid).collection('savedAsteroids').get().then((docs) => {
        docs.forEach((doc) => {
          const asteroidId = doc.get('id');
          asteroidIdsObj[asteroidId] = asteroidId;
          savedAsteroids.push(doc.data());
        });
        commit('setSavedAsteroids', [savedAsteroids, asteroidIdsObj]);
      });
    },

    async saveAsteroid({ dispatch }, asteroid) {
      fb.usersCollection.doc(fb.auth.currentUser.uid).collection('savedAsteroids').doc(asteroid.id).set(asteroid)
        .then(() => {
          dispatch('fetchSavedAsteroids');
        });
    },

    async deleteAsteroid({ dispatch }, asteroid) {
      fb.usersCollection.doc(fb.auth.currentUser.uid).collection('savedAsteroids').doc(asteroid.id).delete()
        .then(() => {
          dispatch('fetchSavedAsteroids');
        })
        .catch((err) => {
          console.log(err);
        });
    },

    async getAsteroids() {
      const asteroidIdObj = {};
      const docs = await usersCollection.doc(auth.currentUser.uid).collection('savedAsteroids').get();
      docs.forEach((doc) => {
        const asteroidId = doc.get('id');
        asteroidIdObj[asteroidId] = asteroidId;
      });
      store.commit('setAsteroids', asteroidIdObj);
    },
  },
  modules: {
  },
});

// const asteroidIdObj = {};
// usersCollection.doc(auth.currentUser.uid).collection('savedAsteroids').get().then((docs) => {
//   docs.forEach((doc) => {
//     console.log('doc is: ', doc);
//     const asteroidId = doc.get('id');
//     asteroidIdObj[asteroidId] = asteroidId;
//   });
//   store.state.asteroidIdObj = asteroidIdObj;
// });

export default store;
